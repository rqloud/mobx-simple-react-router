import React from 'react';
import { observer, inject } from 'mobx-react';
import TextField from 'material-ui/TextField';
import MyComponentChild from './MyComponentChild';

@inject('Store', 'Store2') @observer
class MyComponent extends React.Component {
  render() {
    return (
      <div>
        <p>{this.props.Store.decorated}</p>
        <TextField 
          defaultValue={this.props.Store.name} 
          onChange={(event) => {this.props.Store.name = event.currentTarget.value}}
          id='nameInput1'
        />

        <MyComponentChild />
        <TextField 
          defaultValue={this.props.Store2.nickname} 
          onChange={(event) => {this.props.Store2.nickname = event.currentTarget.value}}
          id='nicknameInput1'
        />
      </div>
    );
  }
}

export default MyComponent;
