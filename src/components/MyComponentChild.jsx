import React from 'react';
import { observer, inject } from 'mobx-react';

@inject('Store2') @observer
class MyComponentChild extends React.Component {
  render() {
    return (
      <div>
        <p>{this.props.Store2.decorated}</p>
      </div>
    );
  }
}

export default MyComponentChild;
