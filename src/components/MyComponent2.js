import React from 'react';
import { observer, inject } from 'mobx-react';
import TextField from 'material-ui/TextField';
import MyComponentChild from './MyComponentChild';

@inject('Store2') @observer
class MyComponent2 extends React.Component {
  render() {
    return (
      <div>
        <p>{this.props.Store2.decorated}</p>
        <TextField 
          defaultValue={this.props.Store2.nickname} 
          onChange={(event) => {this.props.Store2.nickname = event.currentTarget.value}}
          id='nicknameInput2'
        />
      </div>
    );
  }
}

export default MyComponent2;
