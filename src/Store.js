import { observable, computed } from 'mobx';

class Store {
  @observable name = 'Roland';

  @computed get decorated() {
    return `${this.name} is the name.`;
  }
}

export default new Store();
