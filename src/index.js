import React from 'react';
import ReactDOM from 'react-dom';
import Store from './Store';
import Store2 from './Store2';
import App from './components/App';
import MyComponent from './components/MyComponent';
import MyComponent2 from './components/MyComponent2';
import { Provider } from 'mobx-react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

const stores = {Store, Store2};

const routes = (
  <Provider {...stores}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={MyComponent} />
        <Route path="/nick" component={MyComponent2} />
      </Route>
    </Router>    
  </Provider>
)

const root = document.createElement('div');
root.id = 'app';
document.body.appendChild(root);

ReactDOM.render(routes, document.querySelector('#app'));
