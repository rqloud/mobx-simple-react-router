import { observable, computed } from 'mobx';

class Store2 {
  @observable nickname = 'Rolo';

  @computed get decorated() {
    return `${this.nickname} is the nickname.`;
  }
}

export default new Store2();
